var Discord = require("discord.js")
var client = new Discord.Client()

var cfg = require("./cfg")

var rive = new (require("rivescript"));

rive.loadDirectory("rivescripts").then(() => {
    console.log("Loaded RiveScripts.")
    rive.sortReplies()
})

client.on("ready", () => {
    console.log("Authenticated @ Discord.")
})

client.on("message", async (m) => {
    if (m.author.bot||!m.guild||!m.content.startsWith(`<@${client.user.id}>`)) return // make sure the bot doesnt respond to bots, and only to messages starting with a mention
    
    const mention = `<@${client.user.id}>`
    let input = m.content.split(">").slice(1).join(">").trim() // remove the mention and call it ~~a day~~ the inpu

    const reply = await rive.reply(m.author.id, input)
    if (reply.includes("ERR: No Reply Found")) return m.reply("I don't understand this.")

    m.reply(reply)
})

client.login(cfg.token)